package ap.encapsulation;

public class StudentEncapDemo {

	public static void main(String[] args) {

		Student studentRaja = new Student();
		studentRaja.setName("Thushi");
		studentRaja.setAddress("Jaffna");
		studentRaja.setBatch("CSD-16");
		studentRaja.setAge(22);

		Student studentRani = new Student();
		studentRani.setName("Nivetha");
		studentRani.setAddress("Manipay");
		studentRani.setBatch("CSD-17");
		studentRani.setAge(21);

		System.out.println(studentRaja.getName());
		System.out.println(studentRani.getName() + ":" + studentRani.getAge());

		System.out.println(studentRaja);
	}

}
