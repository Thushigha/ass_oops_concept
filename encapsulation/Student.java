package ap.encapsulation;

public class Student {

	private String name;
	private String address;
	private int age;
	private String batch;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@Override
	public String toString() {
		return "student [Name=" + name + ", Address=" + address + ", Age=" + age + " years" + ", Batch=" + batch + "]";
	}

}
