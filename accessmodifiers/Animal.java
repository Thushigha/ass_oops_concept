package ap.accessmodifiers;

public class Animal {
	// public variable
	public int legCount;

	// public method
	public void display() {
		System.out.println("Dog is a animal.");
		System.out.println("It has " + legCount + " legs.");
	}
}
