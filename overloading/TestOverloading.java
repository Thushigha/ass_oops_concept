package ap.overloading;

public class TestOverloading {
	public static void main(String[] args) {
		System.out.println(Adder.add(10, 11));
		System.out.println(Adder.add(22.9, 22.6));
	}
}