package ap.interfac;

public class Rectangle implements Shape {
	@Override
	public double getArea(double w, double h) {
		return w*h;
	}
	@Override
	public double getPerimeter(double w, double h) {
		return 2* (w+h);
		
	}
	
	@Override
	public String getColour() {
		return "Green" ;
		
	}
	@Override
	public boolean isfilled() {
		return false;
	}
	@Override
	public int getEdges() {
		return 4;
	
	}
}
