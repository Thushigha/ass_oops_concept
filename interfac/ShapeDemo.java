package ap.interfac;

public class ShapeDemo {
	public static void main(String[] args) {
		Rectangle rec = new Rectangle();
		System.out.println("It is a " + rec.getColour() + ". Rectangle area is " + rec.getArea(10, 20));

		Square sqr = new Square();
		System.out.println("Square is filled :" + sqr.isfilled());
	}

}
