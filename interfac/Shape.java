package ap.interfac;

public interface Shape {
	public double getArea(double w, double h);

	public double getPerimeter(double w, double h);

	public String getColour();

	public boolean isfilled();

	public int getEdges();
}
