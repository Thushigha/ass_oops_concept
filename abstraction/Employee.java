package ap.abstraction;

public class Employee extends Person {

	private int empId;

	public Employee(String name, String gender, int empId) {
		super(name, gender);
		this.empId = empId;
	}

	@Override
	public void work() {
		if (empId == 0) {
			System.out.println("Not working");
		} else {
			System.out.println("Working as employee!!");
		}
	}
}
