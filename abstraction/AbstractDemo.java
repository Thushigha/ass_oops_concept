package ap.abstraction;

public class AbstractDemo {

	public static void main(String args[]) {
		Person student = new Employee("Nive", "Female", 15);
		Person employee = new Employee("Thushi", "Female", 255);
		student.work();
		employee.work();
		employee.changeName("Thushigha");
		System.out.println(employee.toString());

	}

}
