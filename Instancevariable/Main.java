package ap.Instancevariable;

public class Main {

	public String name;// this instance variable is visible for any child class.

	private int age;// this instance age variable is visible in Record class only.

	public Main(String RecName) {
		name = RecName;
	}

	public void setAge(int RecSal) {
		age = RecSal;
	}

	public void printRec() {
		System.out.println("name : " + name); // print the value for �name�
		System.out.println("age :" + age); // prints the value for �age�
	}

	public static void main(String args[]) {
		Main r = new Main("Thushi");
		r.setAge(22);
		r.printRec();
	}
}
