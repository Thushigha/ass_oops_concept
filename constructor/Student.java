package ap.constructor;

public class Student {

	public String name;
	public int age;

	// default constructor
	public Student() {
	}

	// single parameter constructor
	public Student(String name) {
		this.name = name;
	}

	// multiple parameter constructor
	public Student(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public void printDetails() {
		// System.out.println("This is a Student class");
		System.out.println("Hello " + name + " you are " + age + " years old.");
	}

}
