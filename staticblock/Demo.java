package ap.staticblock;

public class Demo {
	static int x;
	static int y;
	static {
		x = 50;
		y = 100;
	}

	public static void main(String args[]) {
		System.out.println("Value of x = " + x);
		System.out.println("Value of y = " + y);
	}

}
