package ap.inheritance;

public class BasicCalculator {

	public int addNum(int x, int y) {
		return x + y;
	}

	public int subNum(int x, int y) {
		return x - y;
	}

	public int mulNum(int x, int y) {
		return x * y;
	}

	public int divNum(int x, int y) {
		return x / y;
	}

	public int modNum(int x, int y) {
		return x % y;
	}

}
